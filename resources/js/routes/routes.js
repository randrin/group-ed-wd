import Home from "../components/Site/Home";
import About from "../components/Site/About";
import Contact from "../components/Site/Contact";
import Services from "../components/Site/Services";
import Projects from "../components/Site/Projects";
import Dashboard from "../components/Dashboard/Dashboard";
import Login from "../components/Site/auth/Login";
import Error404 from "../components/Site/inc/Error404";
import Registration from "../components/Site/auth/Registration";
import ChangePassword from "../components/Site/partials/ChangePassword";
import ResetPassword from "../components/Site/partials/ResetPassword";
import Faqs from "../components/Site/partials/Faqs";
import Profil from "../components/Site/partials/Profil";
import FaqDashboard from "../components/Dashboard/faq/FaqDashboard";
import FaqCreateDashboard from "../components/Dashboard/faq/FaqCreateDashboard";
import FaqEditDashboard from "../components/Dashboard/faq/FaqEditDashboard";
import ContactDashboard from "../components/Dashboard/contact/ContactDashboard";
import ContactShowDashboard from "../components/Dashboard/contact/ContactShowDashboard";
import CategoryprojectDashboard from "../components/Dashboard/categoryproject/CategoryprojectDashboard";
import ProjectDashboard from "../components/Dashboard/project/ProjectDashboard";
import UsersDashboard from "../components/Dashboard/user/UsersDashboard";
import NewslettersDashboard from "../components/Dashboard/newsletter/NewslettersDashboard";
import CategoryserviceDashboard from "../components/Dashboard/categoryservice/CategoryserviceDashboard";
import ServiceDashboard from "../components/Dashboard/service/ServiceDashboard";
import ServiceCreateDashboard from "../components/Dashboard/service/ServiceCreateDashboard";
import ServiceEditDashboard from "../components/Dashboard/service/ServiceEditDashboard";
import ServicesCategory from "../components/Site/service/ServicesCategory";
import ServicesCategoryShow from "../components/Site/service/ServicesCategoryShow";
import TermeconditionCreateDashboard from "../components/Dashboard/termecondition/TermeconditionCreateDashboard";
import TermeconditionEditDashboard from "../components/Dashboard/termecondition/TermeconditionEditDashboard";
import TermeconditionDashboard from "../components/Dashboard/termecondition/TermeconditionDashboard";
import ProjectCreateDashboard from "../components/Dashboard/project/ProjectCreateDashboard";
import ProjectEditDashboard from "../components/Dashboard/project/ProjectEditDashboard";
import TermsConditions from "../components/Site/partials/TermsConditions";
import CookieDashboard from "../components/Dashboard/cookie/CookieDashboard";
import CookieCreateDashboard from "../components/Dashboard/cookie/CookieCreateDashboard";
import CookieEditDashboard from "../components/Dashboard/cookie/CookieEditDashboard";
import Cookies from "../components/Site/partials/Cookies";
import PolitiquesiteDashboard from "../components/Dashboard/politiquesite/PolitiquesiteDashboard";
import PolitiquesiteCreateDashboard from "../components/Dashboard/politiquesite/PolitiquesiteCreateDashboard";
import PolitiquesiteEditDashboard from "../components/Dashboard/politiquesite/PolitiquesiteEditDashboard";
import PrivaciesPolicies from "../components/Site/partials/PrivaciesPolicies";

export const routes = [
    {path: '/dashboard/', name: 'dashboard.index', component: Dashboard},

    {path: '/dashboard/contacts/', name: 'contacts.index', component: ContactDashboard},
    {path: '/dashboard/contacts/:contact/', name: 'contacts.show', component: ContactShowDashboard},

    {path: '/dashboard/categoryprojects/', name: 'categoryprojects.index', component: CategoryprojectDashboard},

    {path: '/dashboard/projects/', name: 'projects.index', component: ProjectDashboard},
    {path: '/dashboard/projectsites/create/', name: 'projectsites.create', component: ProjectCreateDashboard},
    {path: '/dashboard/projectsites/:projectsite/edit/', name: 'projectsites.edit', component: ProjectEditDashboard},

    {path: '/dashboard/users/', name: 'users.index', component: UsersDashboard},

    {path: '/dashboard/newsletters/', name: 'newsletters.index', component: NewslettersDashboard},

    {path: '/dashboard/categoryservices/', name: 'categoryservices.index', component: CategoryserviceDashboard},

    {path: '/dashboard/servicesites/', name: 'servicesites.index', component: ServiceDashboard},
    {path: '/dashboard/servicesites/create/', name: 'servicesites.create', component: ServiceCreateDashboard},
    {path: '/dashboard/servicesites/:servicesite/edit/', name: 'servicesites.edit', component: ServiceEditDashboard},

    {path: '/dashboard/faqs/', name: 'faqs.index', component: FaqDashboard},
    {path: '/dashboard/faqs/create/', name: 'faqs.create', component: FaqCreateDashboard},
    {path: '/dashboard/faqs/:id/edit/', name: 'faqs.edit', component: FaqEditDashboard},

    {path: '/dashboard/termeconditions/', name: 'termeconditions.index', component: TermeconditionDashboard},
    {path: '/dashboard/termeconditions/create/', name: 'termeconditions.create', component: TermeconditionCreateDashboard},
    {path: '/dashboard/termeconditions/:id/edit/', name: 'termeconditions.edit', component: TermeconditionEditDashboard},

    {path: '/dashboard/cookies/', name: 'cookies.index', component: CookieDashboard},
    {path: '/dashboard/cookies/create/', name: 'cookies.create', component: CookieCreateDashboard},
    {path: '/dashboard/cookies/:id/edit/', name: 'cookies.edit', component: CookieEditDashboard},

    {path: '/dashboard/politiquesites/', name: 'politiquesites.index', component: PolitiquesiteDashboard},
    {path: '/dashboard/politiquesites/create/', name: 'politiquesites.create', component: PolitiquesiteCreateDashboard},
    {path: '/dashboard/politiquesites/:id/edit/', name: 'politiquesites.edit', component: PolitiquesiteEditDashboard},

    {path: '/services/:categoryservice/', name: 'servicesites.category', component: ServicesCategory},
    {path: '/services/:categoryservice/:servicesite', name: 'servicesites_show', component: ServicesCategoryShow},

    {path: '/login', name: 'site.login', component: Login},
    {path: '/register', name: 'site.register', component: Registration},
    {path: '/change/password', name: 'site.changePassword', component: ChangePassword},
    {path: '/reset/password', name: 'site.resetPassword', component: ResetPassword},
    {path: '/', name: 'site', component: Home},
    {path: '/profil/:username', name: 'site.profil', component: Profil},
    {path: '/home', name: 'site.home', component: Home},
    {path: '/about', name: 'site.about', component: About},
    {path: '/contact-us', name: 'site.contact', component: Contact},
    {path: '/nos-services', name: 'site.services', component: Services},
    {path: '/nos-blogs', name: 'site.blog', component: Services},
    {path: '/nos-projects', name: 'site.projects', component: Projects},
    {path: '/nos-faqs', name: 'site.faqs', component: Faqs},
    {path: '/nos-termes-conditions', name: 'site.terms.conditions', component: TermsConditions},
    {path: '/nos-cookies', name: 'site.cookies', component: Cookies},
    {path: '/nos-politiques', name: 'site.privacies', component: PrivaciesPolicies},
    {path: '**', component: Error404}
];

