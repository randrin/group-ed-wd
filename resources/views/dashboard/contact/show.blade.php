@extends('inc.dashboard.main')
<?php $item = htmlspecialchars(config('app.name'));?>
@section('title', $contact->subject.' - '.$item)

@section('style')
@endsection

@section('init')
@endsection

@section('content')
    <div id="app">
        <router-view></router-view>
    </div>
@endsection

@section('script')

@endsection
