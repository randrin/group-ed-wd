<?php

Route::get(
    'api/dashboard/politiquesites',
    'PolitiqueSiteController@api'
)->name('api.dashboard_politiquesites');

Route::get(
    'dashboard/politiquesites',
    'PolitiqueSiteController@index'
)->name('politiquesites.index');

Route::get(
    'dashboard/politiquesites/create',
    'PolitiqueSiteController@create'
)->name('politiquesites.create');

Route::post(
    'dashboard/politiquesites',
    'PolitiqueSiteController@store'
)->name('politiquesites.store');

Route::get(
    'dashboard/politiquesites/{politiquesite:id}/edit',
    'PolitiqueSiteController@edit'
)->name('politiquesites.edit');

Route::get(
    'dashboard/politiquesites/{politiquesite:id}',
    'PolitiqueSiteController@show'
)->name('politiquesites.show');

Route::put(
    'dashboard/politiquesites/{politiquesite:id}',
    'PolitiqueSiteController@update'
)->name('politiquesites.update');

Route::delete(
    'dashboard/politiquesites/{politiquesite:id}/delete',
    'PolitiqueSiteController@destroy'
)->name('politiquesites.destroy');

Route::post(
    'dashboard/politiquesites/{politiquesite:id}/status',
    'PolitiqueSiteController@statusItem'
)->name('status.politiquesites');
