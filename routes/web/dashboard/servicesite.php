
<?php


Route::get(
    'api/dashboard/servicesites', 
    'ServicesiteController@api'
)->name('api.dashboard_servicesites');

Route::get(
    'dashboard/servicesites', 
    'ServicesiteController@index'
)->name('servicesites.index');

Route::get(
    'dashboard/servicesites/create', 
    'ServicesiteController@create'
)->name('servicesites.create');

Route::post(
    'dashboard/servicesites', 
    'ServicesiteController@store'
)->name('servicesites.store');

Route::get(
    'dashboard/servicesites/{servicesite:slugin}/edit',
    'ServicesiteController@edit'
)->name('servicesites.edit');

Route::get(
    'dashboard/servicesites/{servicesite:slugin}',
    'ServicesiteController@show'
)->name('servicesites.show');

Route::put(
    'dashboard/servicesites/{servicesite:slugin}',
    'ServicesiteController@update'
)->name('servicesites.update');

Route::delete(
    'dashboard/servicesites/{servicesite:slugin}/delete',
    'ServicesiteController@destroy'
)->name('servicesites.destroy');

Route::post(
    'dashboard/servicesites/{servicesite:slugin}/status',
    'ServicesiteController@statusItem'
)->name('status.servicesites');
