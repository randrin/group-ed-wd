<?php

Route::get(
    'api/dashboard/cookies',
    'CookieController@api'
)->name('api.dashboard_cookies');

Route::get(
    'dashboard/cookies',
    'CookieController@index'
)->name('cookies.index');

Route::get(
    'dashboard/cookies/create',
    'CookieController@create'
)->name('cookies.create');

Route::post(
    'dashboard/cookies',
    'CookieController@store'
)->name('cookies.store');

Route::get(
    'dashboard/cookies/{cookie:id}/edit',
    'CookieController@edit'
)->name('cookies.edit');

Route::get(
    'dashboard/cookies/{cookie:id}',
    'CookieController@show'
)->name('cookies.show');

Route::put(
    'dashboard/cookies/{cookie:id}',
    'CookieController@update'
)->name('cookies.update');

Route::delete(
    'dashboard/cookies/{cookie:id}/delete',
    'CookieController@destroy'
)->name('cookies.destroy');

Route::post(
    'dashboard/cookies/{cookie:id}/status',
    'CookieController@statusItem'
)->name('status.cookies');
