<?php


Route::get(
    'api/dashboard/projects',
    'ProjectController@api'
)->name('api.dashboard_projects');

Route::get(
    'dashboard/projects',
    'ProjectController@index'
)->name('projects.index');

Route::get(
    'dashboard/projectsites/create',
    'ProjectController@create'
)->name('projects.create');

Route::post(
    'dashboard/projects', 
    'ProjectController@store'
)->name('projects.store');

Route::get(
    'dashboard/projectsites/{projectsite:slugin}/edit',
    'ProjectController@edit'
)->name('projects.edit');

Route::get(
    'dashboard/projectsites/{projectsite:slugin}',
    'ProjectController@show'
)->name('projects.show');

Route::put(
    'dashboard/projects/{project:id}',
    'ProjectController@update'
)->name('projects.update');

Route::delete(
    'dashboard/projects/{project:id}/delete',
    'ProjectController@destroy'
)->name('projects.destroy');

Route::post(
    'dashboard/projects/{project:id}/status',
    'ProjectController@statusItem'
)->name('status.projects');
