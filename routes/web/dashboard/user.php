
<?php


Route::get(
    'api/dashboard/users', 
    'UserController@api'
)->name('api.dashboard_users');

Route::get(
    'dashboard/users', 
    'UserController@index'
)->name('users.index');

Route::post(
    'dashboard/users', 
    'UserController@store'
)->name('users.store');

Route::get(
    'dashboard/users/{user:id}/edit',
    'UserController@edit'
)->name('users.edit');

Route::get(
    'dashboard/users/{user:id}',
    'UserController@show'
)->name('users.show');

Route::put(
    'dashboard/users/{user:id}',
    'UserController@update'
)->name('users.update');

Route::delete(
    'dashboard/users/{user:id}/delete',
    'UserController@destroy'
)->name('users.destroy');

Route::post(
    'dashboard/users/{user:id}/status',
    'UserController@statusItem'
)->name('status.users');
