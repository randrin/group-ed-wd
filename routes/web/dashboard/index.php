<?php

Route::group(['namespace' => 'Dashboard'], function(){

    Route::group(['middleware' => 'verified'],function (){

        Route::group(['middleware' => 'verified_status_admin'],function (){

            Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');

            require(__DIR__ . DIRECTORY_SEPARATOR . 'contact.php');
            require(__DIR__ . DIRECTORY_SEPARATOR . 'categoryservice.php');
            require(__DIR__ . DIRECTORY_SEPARATOR . 'categoryproject.php');
            require(__DIR__ . DIRECTORY_SEPARATOR . 'faq.php');
            require(__DIR__ . DIRECTORY_SEPARATOR . 'termecondition.php');
            require(__DIR__ . DIRECTORY_SEPARATOR . 'cookie.php');
            require(__DIR__ . DIRECTORY_SEPARATOR . 'politiquesite.php');
            require(__DIR__.  DIRECTORY_SEPARATOR . 'servicesite.php');
            require(__DIR__.  DIRECTORY_SEPARATOR . 'newsletter.php');
            require(__DIR__ . DIRECTORY_SEPARATOR . 'project.php');
            require(__DIR__ . DIRECTORY_SEPARATOR . 'user.php');

        });

    });

    Route::get(
        'services/{categoryservice:slug}',
        'ServicesiteController@category'
    )->name('servicesites.category');

    Route::get(
        'api/services/{categoryservice:slug}',
        'ServicesiteController@ipacategory'
    )->name('api.servicesites_category');

    Route::get(
        'services/{categoryservice:slug}/{servicesite:slug}',
        'ServicesiteController@servicesite'
    )->name('servicesites_show');

    Route::get(
        'api/services/{categoryservice:slug}/{servicesite:slug}',
        'ServicesiteController@ipaservicesite'
    )->name('api.servicesites_show');

    Route::get(
        'api/categoryservices',
        'CategoryserviceController@apiSite'
    )->name('api.categoryservices');

    Route::post(
        'newsletters/send',
        'NewsletterController@store'
    )->name('newsletters.store');

});
