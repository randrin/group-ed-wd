<?php


Route::get(
    'api/dashboard/categoryservices',
    'CategoryserviceController@api'
)->name('api.dashboard_categoryservices');

Route::get(
    'api/dashboard/categories/services',
    'CategoryserviceController@apiSite'
)->name('api.dashboard.categoryservices');

Route::get(
    'dashboard/categoryservices',
    'CategoryserviceController@index'
)->name('categoryservices.index');

Route::post(
    'api/dashboard/categoryservices',
    'CategoryserviceController@store'
)->name('categoryservices.store');

Route::put(
    'api/dashboard/categoryservices/{categoryservice:id}',
    'CategoryserviceController@update'
)->name('categoryservices.update');

Route::delete(
    'dashboard/categoryservices/{categoryservice:id}/delete',
    'CategoryserviceController@destroy'
)->name('categoryservices.destroy');

Route::post(
    'dashboard/categoryservices/{categoryservice:id}/status',
    'CategoryserviceController@statusItem'
)->name('status.categoryservices');
