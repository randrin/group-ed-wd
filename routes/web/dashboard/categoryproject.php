<?php


Route::get(
    'api/dashboard/categoryprojects',
    'CategoryprojectController@api'
)->name('api.dashboard_categoryprojects');

Route::get(
    'api/dashboard/categories/projects',
    'CategoryprojectController@apiSite'
)->name('api.dashboard.categoryprojects');

Route::get(
    'dashboard/categoryprojects',
    'CategoryprojectController@index'
)->name('categoryprojects.index');

Route::post(
    'dashboard/categoryprojects',
    'CategoryprojectController@store'
)->name('categoryprojects.store');

Route::put(
    'dashboard/categoryprojects/{categoryproject:id}',
    'CategoryprojectController@update'
)->name('categoryprojects.update');

Route::delete(
    'api/dashboard/categoryprojects/{categoryproject:id}/delete',
    'CategoryprojectController@destroy'
)->name('categoryprojects.destroy');

Route::post(
    'dashboard/categoryprojects/{categoryproject:id}/status',
    'CategoryprojectController@statusItem'
)->name('status.categoryprojects');
