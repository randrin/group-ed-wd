
<?php


Route::get(
    'api/dashboard/termeconditions',
    'TermeconditionController@api'
)->name('api.dashboard_termeconditions');

Route::get(
    'dashboard/termeconditions',
    'TermeconditionController@index'
)->name('termeconditions.index');

Route::get(
    'dashboard/termeconditions/create',
    'TermeconditionController@create'
)->name('termeconditions.create');

Route::post(
    'dashboard/termeconditions',
    'TermeconditionController@store'
)->name('termeconditions.store');

Route::get(
    'dashboard/termeconditions/{termecondition:id}/edit',
    'TermeconditionController@edit'
)->name('termeconditions.edit');

Route::get(
    'dashboard/termeconditions/{termecondition:id}',
    'TermeconditionController@show'
)->name('termeconditions.show');

Route::put(
    'dashboard/termeconditions/{termecondition:id}',
    'TermeconditionController@update'
)->name('termeconditions.update');

Route::delete(
    'dashboard/termeconditions/{termecondition:id}/delete',
    'TermeconditionController@destroy'
)->name('termeconditions.destroy');

Route::post(
    'dashboard/termeconditions/{termecondition:id}/status',
    'TermeconditionController@statusItem'
)->name('status.termeconditions');
