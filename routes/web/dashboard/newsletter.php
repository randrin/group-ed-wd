<?php


Route::get(
    'api/dashboard/newsletters',
    'NewsletterController@api'
)->name('api.dashboard_newsletters');

Route::get(
    'dashboard/newsletters',
    'NewsletterController@index'
)->name('newsletters.index');


