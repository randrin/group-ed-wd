
<?php


Route::get(
    'api/dashboard/faqs', 
    'FaqController@api'
)->name('api.dashboard_faqs');

Route::get(
    'dashboard/faqs', 
    'FaqController@index'
)->name('faqs.index');

Route::get(
    'dashboard/faqs/create', 
    'FaqController@create'
)->name('faqs.create');

Route::post(
    'dashboard/faqs', 
    'FaqController@store'
)->name('faqs.store');

Route::get(
    'dashboard/faqs/{faq:id}/edit',
    'FaqController@edit'
)->name('faqs.edit');

Route::get(
    'dashboard/faqs/{faq:id}',
    'FaqController@show'
)->name('faqs.show');

Route::put(
    'dashboard/faqs/{faq:id}',
    'FaqController@update'
)->name('faqs.update');

Route::delete(
    'dashboard/faqs/{faq:id}/delete',
    'FaqController@destroy'
)->name('faqs.destroy');

Route::post(
    'dashboard/faqs/{faq:id}/status',
    'FaqController@statusItem'
)->name('status.faqs');
