<?php


Route::get(
    'api/dashboard/contacts',
    'ContactController@api'
)->name('api.dashboard_contacts');

Route::get(
    'dashboard/contacts',
    'ContactController@index'
)->name('contacts.index');

Route::get(
    'api/dashboard/contacts/{contact:slug}',
    'ContactController@apishow'
)->name('api.dashboard_contact_show');

Route::get(
    'dashboard/contacts/{contact:slug}',
    'ContactController@show'
)->name('contacts.show');

Route::post(
    'dashboard/contacts/{contact:id}/status',
    'ContactController@statusItem'
)->name('status.contacts');

Route::delete(
    'dashboard/contacts/{contact:id}/delete',
    'ContactController@destroy'
)->name('contacts.destroy');
