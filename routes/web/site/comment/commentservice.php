<?php


Route::post(
    'services/{categoryservice:slug}/{servicesite:slug}/comments',
    'CommentController@commentsend'
)->name('servicesitesendcomment_site');

Route::group(['prefix' => 'api'], function () {

    Route::get(
        'services/{categoryservice:slug}/{servicesite:slug}/comments',
        'CommentController@getcomment'
    )->name('api.servicesitesendcomment_site');
});
