<?php

Route::group(['namespace' => 'Partials'], function(){
    Route::get('/profil/{username}', 'ProfilController@index')->name('groupEdWd.profil');
    Route::get('/nos-faqs', 'FaqsController@index')->name('groupEdWd.faqs');
    Route::get('/api/nos-faqs', 'FaqsController@api')->name('api.faqs');
    Route::get('/nos-termes-conditions', 'TermsConditionsController@index')->name('groupEdWd.terms.conditions');
    Route::get('/api/nos-termsconditions', 'TermsConditionsController@api')->name('api.termsconditions');
    Route::get('/nos-cookies', 'CookiesController@index')->name('groupEdWd.cookies');
    Route::get('/api/nos-cookies', 'CookiesController@api')->name('api.cookies');
    Route::get('/nos-politiques', 'PolitiquesSitesController@index')->name('groupEdWd.privacies');
    Route::get('/api/nos-politiques', 'PolitiquesSitesController@api')->name('api.privacies');
});
