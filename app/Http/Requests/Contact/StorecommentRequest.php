<?php
namespace App\Http\Requests\Contact;

class StorecommentRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getRules('commentsend');
    }

} // class
