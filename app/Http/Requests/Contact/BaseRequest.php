<?php
namespace App\Http\Requests\Contact;

use App\Http\Requests\Request;


class BaseRequest extends Request
{
    // --------------------------------------------------------------------------

    /**
     * Return the array of validation rules for the given group.
     *
     * @param string $group The group name: 'store' or 'update'.
     * @return array
     */
    protected function getRules($group)
    {
        if ($group == 'contactsend') {
            $rules = [
                'name' => 'required|string|max:200',
                'subject' => 'required|string|min:3|max:200',
                'email' => 'required|email|min:3|max:200',
                'message' => 'required|max:50000',
            ];
        }
        elseif ($group === 'commentsend') { // 'edit'
            $rules = [
                'full_name' => 'required|string|max:200',
                'email' => 'required|email|min:3|max:250',
                'sex' => 'required',
                'body' => 'required|max:50000',
            ];
        }
        else { // 'edit'
            $rules = [
               ///
            ];
        }

        return $rules;
    }

    // --------------------------------------------------------------------------

    // --------------------------------------------------------------------------

    /**
     * the attributes method replaces the :attribute placeholder on the validation messages
     * with given attribute names
     *
     * @return array
     */

    public function attributes()
    {
        return [
            //
        ];
    }

    // --------------------------------------------------------------------------

    // --------------------------------------------------------------------------

} // class
