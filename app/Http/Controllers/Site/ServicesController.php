<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Servicesite;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('site.services');
    }

    public function apiservices()
    {

        $services = Servicesite::with('user','categoryservice')
            ->where('status',1)
            ->orderBy('created_at', 'ASC')
            ->latest()
            ->get();

        return response()->json($services,200);
    }
}
