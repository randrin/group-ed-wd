<?php

namespace App\Http\Controllers\Site;

use App\Models\Project;
use App\Models\Categoryproject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('site.projects');
    }

    public function apiprojects()
    {

        $projects = Project::with('user','categoryproject')
            ->where('status',1)
            ->orderBy('created_at', 'ASC')
            ->latest()
            ->get();

        return response()->json($projects,200);
    }

    public function apicategoryprojects()
    {
        $categoryprojects = Categoryproject::where('status',1)->latest()->get();

        return response()->json($categoryprojects,200);
    }
}
