<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\StorecommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Categoryservice;
use App\Models\Comment;
use App\Models\Servicesite;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function getcomment($categoryservice,Servicesite $servicesite)
    {
        $comments = CommentResource::collection($servicesite->comments()
            ->with('commentable')
            ->whereIn('commentable_id',[$servicesite->id])
            ->where(['status'=> 1])
            ->where('commentable_type',Servicesite::class)
            ->orderByDesc('created_at')->distinct()->get());

        return response()->json($comments,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function commentsend(StorecommentRequest $request,$categoryservice,Servicesite $servicesite)
    {
        $comment = $servicesite->comments()->create($request->all());

        return response()->json($comment,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
