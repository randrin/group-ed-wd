<?php

namespace App\Http\Controllers\Site\Partials;

use App\Http\Controllers\Controller;
use App\Models\Termecondition;
use Illuminate\Http\Response;

class TermsConditionsController extends Controller
{

    public function api()
    {
        $termesconditions = Termecondition::with('user')
            ->where('status',1)
            ->orderBy('created_at', 'ASC')
            ->latest()
            ->get();

        return response()->json($termesconditions,200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('site.partials.termsconditions');
    }
}
