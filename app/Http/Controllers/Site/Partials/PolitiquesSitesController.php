<?php

namespace App\Http\Controllers\Site\Partials;

use App\Http\Controllers\Controller;
use App\Models\Politiquesite;
use Illuminate\Http\Response;

class PolitiquesSitesController extends Controller
{
    public function api()
    {
        $politiques = Politiquesite::with('user')
            ->where('status', 1)
            ->orderBy('created_at', 'ASC')
            ->latest()
            ->get();

        return response()->json($politiques, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('site.partials.politiques');
    }
}
