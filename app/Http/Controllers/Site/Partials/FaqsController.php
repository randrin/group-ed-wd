<?php

namespace App\Http\Controllers\Site\Partials;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\Response;

class FaqsController extends Controller
{
    public function api()
    {
        $faqs = Faq::with('user')
            ->where('status',1)
            ->orderBy('created_at', 'ASC')
            ->latest()
            ->get();

        return response()->json($faqs,200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('site.partials.faqs');
    }
}
