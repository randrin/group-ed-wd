<?php

namespace App\Http\Controllers\Site\Partials;

use App\Http\Controllers\Controller;
use App\Models\Cookie;
use Illuminate\Http\Response;

class CookiesController extends Controller
{
    public function api()
    {
        $cookies = Cookie::with('user')
            ->where('status', 1)
            ->orderBy('created_at', 'ASC')
            ->latest()
            ->get();

        return response()->json($cookies, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('site.partials.cookies');
    }
}
