<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Termecondition;
use Illuminate\Http\Request;


class TermeconditionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function api()
    {
        $termecondition = Termecondition::with('user')->latest()->get();

        return response()->json($termecondition,200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.termecondition.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.termecondition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|string|min:2|max:200',
            'description'=>'required|string|min:2',
        ]);

        $inputs = $request->all();

        $data = Termecondition::create($inputs);

        return response()->json($data,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  Termecondition  $termecondition
     * @return \Illuminate\Http\Response
     */
    public function show(Termecondition $termecondition)
    {
        $data = Termecondition::whereId($termecondition->id)
        ->with('user')
        ->first();

        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Termecondition $termecondition)
    {
        return view('dashboard.termecondition.edit',['termecondition' => $termecondition]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Termecondition  $termecondition
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Termecondition $termecondition)
    {
        $this->validate($request,[
            'title'=>'required|string|min:2|max:200',
            'description'=>'required|string|min:2',
        ]);

        $termecondition->update($request->all());

        return ['redirect' => route('termeconditions.index')];
    }

    public function statusItem(Termecondition $termecondition)
    {

        $data = $termecondition->update(['status' => !$termecondition->status,]);

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Termecondition  $termecondition
     * @return \Illuminate\Http\Response
     */
    public function destroy(Termecondition $termecondition)
    {
        $termecondition->delete();

        return ['message' => 'Deleted successfully'];
    }
}
