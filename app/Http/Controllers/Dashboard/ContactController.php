<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;


class ContactController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function api()
    {
        $contacts = Contact::latest()->get();

        return response()->json($contacts,200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.contact.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        return view('dashboard.contact.show',compact('contact'));
    }

    public function apishow(Contact $contact)
    {
        $data = Contact::whereSlug($contact->slug)
            ->with('user')
            ->first();

        return response()->json($data,200);

    }

    /**
     * @param Contact $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function statusItem(Contact $contact)
    {
        $data = $contact->update(['status' => !$contact->status,]);

        return response()->json($data, 200);
    }

    /**
     * @param Contact $contact
     * @return array
     * @throws \Exception
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();

        return ['message' => 'Deleted successfully'];
    }
}
