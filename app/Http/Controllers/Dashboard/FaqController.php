<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class FaqController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function api()
    {
        $faqs = Faq::with('user')->latest()->get();

        return response()->json($faqs,200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.faq.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|string|min:2|max:200',
            'description' => 'required|string|min:2',
        ]);

        $inputs = $request->all();

        $data = Faq::create($inputs);

        return response()->json($data,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  Faq  $faq
     * @return Response
     */
    public function show(Faq $faq)
    {
        $data = Faq::whereId($faq->id)
        ->with('user')
        ->first();

        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Faq  $faq
     * @return Response
     */
    public function edit(Faq $faq)
    {
        return view('dashboard.faq.edit',['faq' => $faq]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  Faq  $faq
     * @return Response
     */

    public function update(Request $request, Faq $faq)
    {
        $this->validate($request,[
            'title'=>'required|string|min:2|max:200',
            'description' => 'required|string|min:2',
        ]);

        $faq->update($request->all());

        return ['redirect' => route('faqs.index')];
    }

    public function statusItem(Faq $faq)
    {

        $data = $faq->update(['status' => !$faq->status,]);

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Faq  $faq
     * @return Response
     */
    public function destroy(Faq $faq)
    {
        $faq->delete();

        return ['message' => 'Deleted successfully'];
    }
}
