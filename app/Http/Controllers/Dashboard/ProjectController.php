<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;


class ProjectController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function api()
    {
        $projects = Project::with('user','categoryproject')->latest()->get();

        return response()->json($projects,200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.project.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|string|min:2|max:200',
            'description' => 'required|string|min:2',
            'photo' => 'required',
        ]);


        if($request->photo){

            $image = $request->photo;
            $imageExt = explode(";",explode('/', $image)[1])[0];
            $imageName = sha1(date('YmdHis') . str_random(30)) . '.' . $imageExt;
            $dir = 'assets/images/tmp/project/';
            if(!file_exists($dir)){mkdir($dir, 0775, true);}
            $filenametostore = 'assets/images/tmp/project/' . $imageName;
            $imagedecode = base64_decode(explode(",", $image)[1]);

            Image::make($imagedecode)->fit(1200,703)->save($filenametostore);
            $myfilename = "/assets/images/tmp/project/{$imageName}";
        }

        $data = Project::create([
            'photo' => $myfilename,
            'title' => $request->title,
            'description' => $request->description,
            'categoryproject_id' => $request->categoryproject_id,

        ]);

        return response()->json($data,200);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  Project  $project
     * @return Response
     */
    public function edit(Project $project)
    {
        return view('dashboard.project.edit',['project' => $project]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  Project  $project
     * @return Response
     */

    /**
     * Display the specified resource.
     *
     * @param Project $projectsite
     * @return Response
     */
    public function show(Project $projectsite)
    {
        $data = Project::whereSlugin($projectsite->slugin)
            ->with('user', 'categoryproject')
            ->first();

        return response()->json($data, 200);
    }

    public function update(Request $request, Project $project)
    {
        $this->validate($request,[
            'title'=>'required|string|min:2|max:200',
            'description' => 'required|string|min:2',
            'photo' => 'required',
        ]);

        $currentPhoto = $project->photo;

        if($request->photo != $currentPhoto){

            $image = $request->photo;
            $imageExt = explode(";",explode('/', $image)[1])[0];
            $imageName = sha1(date('YmdHis') . str_random(30)) . '.' . $imageExt;
            $dir = 'assets/images/tmp/project/';
            if(!file_exists($dir)){mkdir($dir, 0775, true);}
            $filenametostore = 'assets/images/tmp/project/' . $imageName;
            $imagedecode = base64_decode(explode(",", $image)[1]);
            Image::make($imagedecode)->fit(1200,703)->save($filenametostore);
            $myfilename = "/assets/images/tmp/project/{$imageName}";

            $request->merge(['photo' =>  $myfilename]);
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }

        $project->slug = null;
        $project->update($request->all());

        return ['redirect' => route('projects.index')];
    }

    public function statusItem(Project $project)
    {

        $data = $project->update(['status' => !$project->status,]);

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Project  $project
     * @return Response
     */
    public function destroy(Project $project)
    {
        $oldFilename = $project->photo;
        File::delete(public_path($oldFilename));

        $project->delete();

        return ['message' => 'Deleted successfully'];
    }
}
