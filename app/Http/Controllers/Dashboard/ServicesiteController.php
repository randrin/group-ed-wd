<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Categoryservice;
use App\Models\Servicesite;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;


class ServicesiteController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth',['except' => [
            'category','ipacategory','servicesite','ipaservicesite'
        ]]);
    }

    public function api()
    {
        $servicesites = Servicesite::with('user','categoryservice')->latest()->get();

        return response()->json($servicesites,200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.servicesite.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.servicesite.create');
    }

    public function category(Categoryservice $categoryservice)
    {
        return view('site.service.services',compact('categoryservice'));
    }

    public function ipacategory(Categoryservice $categoryservice)
    {
        $data = Categoryservice::whereSlug($categoryservice->slug)
        ->with(['servicesites' => function ($q) use ($categoryservice){
              $q->where(['status' => 1])
                ->with('user','categoryservice')
                ->whereIn('categoryservice_id',[$categoryservice->id])
                ->whereHas('categoryservice', function ($q) {$q->where('status',1);})
                ->get();
        }])
        ->first();

        return response()->json($data, 200);
    }

    public function servicesite(Categoryservice $categoryservice,Servicesite $servicesite)
    {
        return view('site.service.services',compact('categoryservice','servicesite'));
    }

    public function ipaservicesite(Categoryservice $categoryservice,Servicesite $servicesite)
    {
        $data = Servicesite::whereSlug($servicesite->slug)
        ->whereIn('categoryservice_id',[$categoryservice->id])
        ->with('user','categoryservice')->first();

        return response()->json($data, 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|string|min:2|max:200',
            'description' => 'required|string|min:2',
            'photo'=>'required',
        ]);

        $inputs = $request->all();

        if($request->photo){

            $image = $request->photo;
            $imageExt = explode(";",explode('/', $image)[1])[0];
            $imageName = sha1(date('YmdHis') . str_random(30)) . '.' . $imageExt;
            $dir = 'assets/images/tmp/servicesite/';
            if(!file_exists($dir)){mkdir($dir, 0775, true);}
            $filenametostore = 'assets/images/tmp/servicesite/' . $imageName;
            $imagedecode = base64_decode(explode(",", $image)[1]);

            Image::make($imagedecode)->fit(1200,703)->save($filenametostore);
            $myfilename = "/assets/images/tmp/servicesite/{$imageName}";

        }

        $data = Servicesite::create([
            'photo' => $myfilename,
            'title' => $inputs['title'],
            'description' => $inputs['description'],
            'categoryservice_id' => $inputs['categoryservice_id'],

        ]);

        return response()->json($data,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  Servicesite  $servicesite
     * @return Response
     */
    public function show(Servicesite $servicesite)
    {
        $data = Servicesite::whereSlugin($servicesite->slugin)
        ->with('user','categoryservice')
        ->first();

        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Servicesite  $servicesite
     * @return Response
     */
    public function edit(Servicesite $servicesite)
    {
        return view('dashboard.servicesite.edit',['servicesite' => $servicesite]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  Servicesite  $servicesite
     * @return string
     */

    public function update(Request $request, Servicesite $servicesite)
    {
        $this->validate($request,[
            'title'=>'required|string|min:2|max:200',
            'description' => 'required|string|min:2',
            'photo'=>'required',
        ]);

        $inputs = $request->all();

        $currentPhoto = $servicesite->photo;

        if($request->photo != $currentPhoto){

            $image = $request->photo;
            $imageExt = explode(";",explode('/', $image)[1])[0];
            $imageName = sha1(date('YmdHis') . str_random(30)) . '.' . $imageExt;
            $dir = 'assets/images/tmp/servicesite/';
            if(!file_exists($dir)){mkdir($dir, 0775, true);}
            $filenametostore = 'assets/images/tmp/servicesite/' . $imageName;
            $imagedecode = base64_decode(explode(",", $image)[1]);
            Image::make($imagedecode)->fit(1200,703)->save($filenametostore);
            $myfilename = "/assets/images/tmp/servicesite/{$imageName}";

            $request->merge(['photo' =>  $myfilename]);
            $oldFilename = $currentPhoto;
            File::delete(public_path($oldFilename));
        }


        $servicesite->slug = null;
        $servicesite->update($request->all());

        return ['redirect' => route('servicesites.index')];
    }

    public function statusItem(Servicesite $servicesite)
    {

        $data = $servicesite->update(['status' => !$servicesite->status,]);

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Servicesite  $servicesite
     * @return Response
     */
    public function destroy(Servicesite $servicesite)
    {
        $oldFilename = $servicesite->photo;
        File::delete(public_path($oldFilename));

        $servicesite->delete();

        return ['message' => 'Deleted successfully'];
    }
}
