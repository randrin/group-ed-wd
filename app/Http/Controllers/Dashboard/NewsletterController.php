<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Newsletter;
use Illuminate\Http\Request;


class NewsletterController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth',['except' => [
            'store',
        ]]);
    }

    public function api()
    {
        $newsletter = Newsletter::latest()->get();

        return response()->json($newsletter,200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.newsletter.index');
    }

        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'email'=> "required|string|email|min:2|max:200|unique:newsletters",
        ]);

        $inputs = $request->all();

        $data = newsletter::create([
            'email' => $inputs['email'],
        ]);

        return response()->json($data,200);
    }


}
