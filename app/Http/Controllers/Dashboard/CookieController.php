<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Cookie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CookieController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function api()
    {
        $cookie = Cookie::with('user')->latest()->get();

        return response()->json($cookie, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.cookie.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.cookie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|min:2|max:200',
            'description' => 'required|string|min:2',
        ]);

        $inputs = $request->all();

        $data = Cookie::create($inputs);

        return response()->json($data, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(Cookie $cookie)
    {
        $data = Cookie::whereId($cookie->id)
            ->with('user')
            ->first();

        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(Cookie $cookie)
    {
        return view('dashboard.cookie.edit', ['cookie' => $cookie]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Cookie $cookie)
    {
        $this->validate($request, [
            'title' => 'required|string|min:2|max:200',
            'description' => 'required|string|min:2',
        ]);

        $cookie->update($request->all());

        return ['redirect' => route('cookies.index')];
    }

    public function statusItem(Cookie $cookie)
    {

        $data = $cookie->update(['status' => !$cookie->status]);

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(Cookie $cookie)
    {
        $cookie->delete();

        return ['message' => 'Deleted successfully'];
    }
}
