<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Categoryproject;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class CategoryprojectController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth',['except' => [
            'apiSite',
        ]]);
    }

    public function api()
    {
        $categoryprojects = Categoryproject::latest()->get();

        return response()->json($categoryprojects,200);
    }

    public function apiSite()
    {
        $categoryprojects = Categoryproject::where('status', 1)->orderBy('created_at', 'ASC')->latest()->get();

        return response()->json($categoryprojects, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.categoryproject.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.categoryproject.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|min:2|max:200|unique:categoryprojects',
            'label'=>'required|string|min:2|max:200|unique:categoryprojects',
        ]);

        $inputs = $request->all();

        $data = Categoryproject::create($inputs);

        return response()->json($data,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  Categoryproject  $categoryproject
     * @return Response
     */
    public function show(Categoryproject $categoryproject)
    {
        $data = Categoryproject::whereId($categoryproject->id)
        ->first();

        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Categoryproject  $categoryproject
     * @return Response
     */
    public function edit(Categoryproject $categoryproject)
    {
        return view('dashboard.categoryproject.edit',['categoryproject' => $categoryproject]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  Categoryproject  $categoryproject
     * @return Response
     */

    public function update(Request $request, Categoryproject $categoryproject)
    {
        $this->validate($request,[
            'name'=> "required|string|min:2|max:200|unique:categoryprojects,name,{$categoryproject->id}",
            'label' => 'required|string|min:2|max:200',
        ]);
        $inputs = $request->all();

        $categoryproject->update([
            'name' => $inputs['name'],
            'label' => $inputs['label'],
            'slug' => null,
        ]);

        return ['redirect' => route('categoryprojects.index')];
    }

    public function statusItem(Categoryproject $categoryproject)
    {

        $data = $categoryproject->update(['status' => !$categoryproject->status,]);

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Categoryproject  $categoryproject
     * @return Response
     */
    public function destroy(Categoryproject $categoryproject)
    {
        $categoryproject->delete();

        return ['message' => 'Deleted successfully'];
    }
}
