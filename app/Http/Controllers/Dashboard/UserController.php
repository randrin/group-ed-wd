<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function api()
    {
        $users = User::latest()->get();

        return response()->json($users,200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => "required|string|min:2|max:50|unique:users",
            'username'=> "required|string|min:2|max:200|unique:users",
            'email'=> "required|string|email|min:2|max:200|unique:users",
            'sex' => "required",
        ]);

        $inputs = $request->all();

        $data = User::create([
            'name' => $inputs['name'],
            'email' => $inputs['email'],
            'username' => $inputs['username'],
            'password' => Hash::make(sha1(('YmdHis') . str_random(30))),
        ]);

        return response()->json($data,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     * @return Response
     */
    public function show(User $user)
    {
        $data = User::whereId($user->id)
        ->first();

        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return Response
     */
    public function edit(User $user)
    {
        return view('dashboard.User.edit',['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  User  $user
     * @return Response
     */

    public function update(Request $request, User $user)
    {
        $this->validate($request,[
            'name' => "required|string|min:2|max:50",
            'username'=> "required|string|min:2|max:200|unique:users,username,{$user->id}",
            'email'=> "required|string|email|min:2|max:200|unique:users,email,{$user->id}",
        ]);

        $user->update($request->all());

        return ['redirect' => route('users.index')];
    }

    public function statusItem(User $user)
    {

        $data = $user->update(['status_user' => !$user->status_user,]);

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return ['message' => 'Deleted successfully'];
    }
}
