<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Politiquesite;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PolitiqueSiteController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function api()
    {
        $privacies = Politiquesite::with('user')->latest()->get();

        return response()->json($privacies, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.politiquesite.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.politiquesite.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|min:2|max:200',
            'description' => 'required|string|min:2',
        ]);

        $inputs = $request->all();

        $data = Politiquesite::create($inputs);

        return response()->json($data, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Politiquesite $politiquesite
     * @return Response
     */
    public function show(Politiquesite $politiquesite)
    {
        $data = Politiquesite::whereId($politiquesite->id)
            ->with('user')
            ->first();

        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Politiquesite $politiquesite
     * @return Response
     */
    public function edit(Politiquesite $politiquesite)
    {
        return view('dashboard.politiquesite.edit', ['privacy' => $politiquesite]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Politiquesite $politiquesite
     * @return Response
     */
    public function update(Request $request, Politiquesite $politiquesite)
    {
        $this->validate($request, [
            'title' => 'required|string|min:2|max:200',
            'description' => 'required|string|min:2',
        ]);

        $politiquesite->update($request->all());

        return ['redirect' => route('politiquesites.index')];
    }

    public function statusItem(Politiquesite $politiquesite)
    {

        $data = $politiquesite->update(['status' => !$politiquesite->status]);

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Politiquesite $politiquesite
     * @return Response
     */
    public function destroy(Politiquesite $politiquesite)
    {
        $politiquesite->delete();

        return ['message' => 'Deleted successfully'];
    }
}
