<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Categoryservice;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class CategoryserviceController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth',['except' => [
            'apiSite',
        ]]);
    }

    public function api()
    {
        $categoryservices = Categoryservice::orderBy('created_at', 'ASC')->latest()->get();

        return response()->json($categoryservices,200);
    }

    public function apiSite()
    {
        $categoryservices = Categoryservice::where('status', 1)->orderBy('created_at', 'ASC')->latest()->get();

        return response()->json($categoryservices,200);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.categoryservice.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.categoryservice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|min:2|max:200|unique:categoryservices',
            'label'=>'required|string|min:2|max:200|unique:categoryservices',
        ]);

        $inputs = $request->all();

        $data = Categoryservice::create($inputs);

        return response()->json($data,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  Categoryservice  $categoryservice
     * @return Response
     */
    public function show(Categoryservice $categoryservice)
    {
        $data = Categoryservice::whereId($categoryservice->id)
        ->first();

        return response()->json($data, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Categoryservice  $categoryservice
     * @return Response
     */
    public function edit(Categoryservice $categoryservice)
    {
        return view('dashboard.categoryservice.edit',['categoryservice' => $categoryservice]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  Categoryservice  $categoryservice
     * @return Response
     */

    public function update(Request $request, Categoryservice $categoryservice)
    {
        $this->validate($request,[
            'name'=> "required|string|min:2|max:200|unique:categoryservices,name,{$categoryservice->id}",
            'label' => 'required|string|min:2|max:200',
        ]);

        $inputs = $request->all();


        $categoryservice->update([
            'name' => $inputs['name'],
            'label' => $inputs['label'],
            'slug' => null,

        ]);

        return ['redirect' => route('categoryservices.index')];
    }

    public function statusItem(Categoryservice $categoryservice)
    {

        $data = $categoryservice->update(['status' => !$categoryservice->status,]);

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Categoryservice  $categoryservice
     * @return Response
     */
    public function destroy(Categoryservice $categoryservice)
    {
        $categoryservice->delete();

        return ['message' => 'Deleted successfully'];
    }
}
