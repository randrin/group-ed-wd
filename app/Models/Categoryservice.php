<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoryservice extends Model
{
    use HasFactory, Sluggable;

    protected $table = 'categoryservices';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model){
            $model->slug = str_slug($model->name);
        });


        static::updating(function($model){
            $model->slug = str_slug($model->name);
        });
    }

    public function servicesites()
    {
        return $this->hasMany(Servicesite::class, 'categoryservice_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
                'separator' => '-'
            ]
        ];
    }
}
