<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Servicesite extends Model
{
    use HasFactory;

    protected $table = 'servicesites';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function categoryservice()
    {
        return $this->belongsTo(Categoryservice::class, 'categoryservice_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model){
            if (auth()->check()){
                $model->user_id = auth()->id();
                $model->ip = request()->ip();
                $model->slugin = Str::uuid();
            }
        });


        static::updating(function($model){
            if (auth()->check()){
                $model->user_id = auth()->id();
                $model->ip = request()->ip();
            }
        });
    }

    public function comments()
    {
        return $this->morphMany(Comment::class ,'commentable');
    }

    protected $casts = [
        'status' => 'boolean',
    ];

    use Sluggable;
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
                'separator' => '-'
            ]
        ];
    }
}
